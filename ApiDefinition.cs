﻿using System;
using CoreGraphics;
using Foundation;
using ObjCRuntime;
using QQ_SDK_Xamarin_iOS;
using UIKit;

namespace QQ_SDK_Xamarin_iOS
{
	// @interface QQApiObject : NSObject
	[BaseType(typeof(NSObject))]
	interface QQApiObject
	{
		// @property (retain, nonatomic) NSString * title;
		[Export("title", ArgumentSemantic.Retain)]
		string Title { get; set; }

		// @property (retain, nonatomic) NSString * description;
		[Export("description", ArgumentSemantic.Retain)]
		string Description { get; set; }

		// @property (assign, nonatomic) uint64_t cflag;
		[Export("cflag")]
		ulong Cflag { get; set; }
	}

	// @interface QQApiResultObject : QQApiObject
	[BaseType(typeof(QQApiObject))]
	interface QQApiResultObject
	{
		// @property (retain, nonatomic) NSString * error;
		[Export("error", ArgumentSemantic.Retain)]
		string Error { get; set; }

		// @property (retain, nonatomic) NSString * errorDescription;
		[Export("errorDescription", ArgumentSemantic.Retain)]
		string ErrorDescription { get; set; }

		// @property (retain, nonatomic) NSString * extendInfo;
		[Export("extendInfo", ArgumentSemantic.Retain)]
		string ExtendInfo { get; set; }
	}

	// @interface QQApiTextObject : QQApiObject
	[BaseType(typeof(QQApiObject))]
	interface QQApiTextObject
	{
		// @property (retain, nonatomic) NSString * text;
		[Export("text", ArgumentSemantic.Retain)]
		string Text { get; set; }

		// -(id)initWithText:(NSString *)text;
		[Export("initWithText:")]
		IntPtr Constructor(string text);

		// +(id)objectWithText:(NSString *)text;
		[Static]
		[Export("objectWithText:")]
		NSObject ObjectWithText(string text);
	}

	// @interface QQApiURLObject : QQApiObject
	[BaseType(typeof(QQApiObject))]
	interface QQApiURLObject
	{
		// @property (nonatomic) QQApiURLTargetType targetContentType;
		[Export("targetContentType", ArgumentSemantic.Assign)]
		QQApiURLTargetType TargetContentType { get; set; }

		// @property (retain, nonatomic) NSURL * url;
		[Export("url", ArgumentSemantic.Retain)]
		NSUrl Url { get; set; }

		// @property (retain, nonatomic) NSData * previewImageData;
		[Export("previewImageData", ArgumentSemantic.Retain)]
		NSData PreviewImageData { get; set; }

		// @property (retain, nonatomic) NSURL * previewImageURL;
		[Export("previewImageURL", ArgumentSemantic.Retain)]
		NSUrl PreviewImageURL { get; set; }

		// -(id)initWithURL:(NSURL *)url title:(NSString *)title description:(NSString *)description previewImageData:(NSData *)data targetContentType:(QQApiURLTargetType)targetContentType;
		[Export("initWithURL:title:description:previewImageData:targetContentType:")]
		IntPtr Constructor(NSUrl url, string title, string description, NSData data, QQApiURLTargetType targetContentType);

		// -(id)initWithURL:(NSURL *)url title:(NSString *)title description:(NSString *)description previewImageURL:(NSURL *)previewURL targetContentType:(QQApiURLTargetType)targetContentType;
		[Export("initWithURL:title:description:previewImageURL:targetContentType:")]
		IntPtr Constructor(NSUrl url, string title, string description, NSUrl previewURL, QQApiURLTargetType targetContentType);

		// +(id)objectWithURL:(NSURL *)url title:(NSString *)title description:(NSString *)description previewImageData:(NSData *)data targetContentType:(QQApiURLTargetType)targetContentType;
		[Static]
		[Export("objectWithURL:title:description:previewImageData:targetContentType:")]
		NSObject ObjectWithURL(NSUrl url, string title, string description, NSData data, QQApiURLTargetType targetContentType);

		// +(id)objectWithURL:(NSURL *)url title:(NSString *)title description:(NSString *)description previewImageURL:(NSURL *)previewURL targetContentType:(QQApiURLTargetType)targetContentType;
		[Static]
		[Export("objectWithURL:title:description:previewImageURL:targetContentType:")]
		NSObject ObjectWithURL(NSUrl url, string title, string description, NSUrl previewURL, QQApiURLTargetType targetContentType);
	}

	// @interface QQApiExtendObject : QQApiObject
	[BaseType(typeof(QQApiObject))]
	interface QQApiExtendObject
	{
		// @property (retain, nonatomic) NSData * data;
		[Export("data", ArgumentSemantic.Retain)]
		NSData Data { get; set; }

		// @property (retain, nonatomic) NSData * previewImageData;
		[Export("previewImageData", ArgumentSemantic.Retain)]
		NSData PreviewImageData { get; set; }

		// @property (retain, nonatomic) NSArray * imageDataArray;
		[Export("imageDataArray", ArgumentSemantic.Retain)]
		NSObject[] ImageDataArray { get; set; }

		// -(id)initWithData:(NSData *)data previewImageData:(NSData *)previewImageData title:(NSString *)title description:(NSString *)description;
		[Export("initWithData:previewImageData:title:description:")]
		IntPtr Constructor(NSData data, NSData previewImageData, string title, string description);

		// -(id)initWithData:(NSData *)data previewImageData:(NSData *)previewImageData title:(NSString *)title description:(NSString *)description imageDataArray:(NSArray *)imageDataArray;
		[Export("initWithData:previewImageData:title:description:imageDataArray:")]
		IntPtr Constructor(NSData data, NSData previewImageData, string title, string description, NSObject[] imageDataArray);

		// +(id)objectWithData:(NSData *)data previewImageData:(NSData *)previewImageData title:(NSString *)title description:(NSString *)description;
		[Static]
		[Export("objectWithData:previewImageData:title:description:")]
		NSObject ObjectWithData(NSData data, NSData previewImageData, string title, string description);

		// +(id)objectWithData:(NSData *)data previewImageData:(NSData *)previewImageData title:(NSString *)title description:(NSString *)description imageDataArray:(NSArray *)imageDataArray;
		[Static]
		[Export("objectWithData:previewImageData:title:description:imageDataArray:")]
		NSObject ObjectWithData(NSData data, NSData previewImageData, string title, string description, NSObject[] imageDataArray);
	}

	// @interface QQApiImageObject : QQApiExtendObject
	[BaseType(typeof(QQApiExtendObject))]
	interface QQApiImageObject
	{
	}

	// @interface QQApiImageArrayForQZoneObject : QQApiObject
	[BaseType(typeof(QQApiObject))]
	interface QQApiImageArrayForQZoneObject
	{
		// @property (retain, nonatomic) NSArray * imageDataArray;
		[Export("imageDataArray", ArgumentSemantic.Retain)]
		NSObject[] ImageDataArray { get; set; }

		// -(id)initWithImageArrayData:(NSArray *)imageDataArray title:(NSString *)title;
		[Export("initWithImageArrayData:title:")]
		IntPtr Constructor(NSObject[] imageDataArray, string title);

		// +(id)objectWithimageDataArray:(NSArray *)imageDataArray title:(NSString *)title;
		[Static]
		[Export("objectWithimageDataArray:title:")]
		NSObject ObjectWithimageDataArray(NSObject[] imageDataArray, string title);
	}

	// @interface QQApiVideoForQZoneObject : QQApiObject
	[BaseType(typeof(QQApiObject))]
	interface QQApiVideoForQZoneObject
	{
		// @property (retain, nonatomic) NSString * assetURL;
		[Export("assetURL", ArgumentSemantic.Retain)]
		string AssetURL { get; set; }

		// -(id)initWithAssetURL:(NSString *)assetURL title:(NSString *)title;
		[Export("initWithAssetURL:title:")]
		IntPtr Constructor(string assetURL, string title);

		// +(id)objectWithAssetURL:(NSString *)assetURL title:(NSString *)title;
		[Static]
		[Export("objectWithAssetURL:title:")]
		NSObject ObjectWithAssetURL(string assetURL, string title);
	}

	// @interface QQApiWebImageObject : QQApiObject
	[BaseType(typeof(QQApiObject))]
	interface QQApiWebImageObject
	{
		// @property (retain, nonatomic) NSURL * previewImageURL;
		[Export("previewImageURL", ArgumentSemantic.Retain)]
		NSUrl PreviewImageURL { get; set; }

		// -(id)initWithPreviewImageURL:(NSURL *)previewImageURL title:(NSString *)title description:(NSString *)description;
		[Export("initWithPreviewImageURL:title:description:")]
		IntPtr Constructor(NSUrl previewImageURL, string title, string description);

		// +(id)objectWithPreviewImageURL:(NSURL *)previewImageURL title:(NSString *)title description:(NSString *)description;
		[Static]
		[Export("objectWithPreviewImageURL:title:description:")]
		NSObject ObjectWithPreviewImageURL(NSUrl previewImageURL, string title, string description);
	}

	// @interface QQApiGroupTribeImageObject : QQApiImageObject
	[BaseType(typeof(QQApiImageObject))]
	interface QQApiGroupTribeImageObject
	{
		// @property (retain, nonatomic) NSString * bid;
		[Export("bid", ArgumentSemantic.Retain)]
		string Bid { get; set; }

		// @property (retain, nonatomic) NSString * bname;
		[Export("bname", ArgumentSemantic.Retain)]
		string Bname { get; set; }
	}

	// @interface QQApiFileObject : QQApiExtendObject
	[BaseType(typeof(QQApiExtendObject))]
	interface QQApiFileObject
	{
		// @property (retain, nonatomic) NSString * fileName;
		[Export("fileName", ArgumentSemantic.Retain)]
		string FileName { get; set; }
	}

	// @interface QQApiAudioObject : QQApiURLObject
	[BaseType(typeof(QQApiURLObject))]
	interface QQApiAudioObject
	{
		// @property (retain, nonatomic) NSURL * flashURL;
		[Export("flashURL", ArgumentSemantic.Retain)]
		NSUrl FlashURL { get; set; }

		// +(id)objectWithURL:(NSURL *)url title:(NSString *)title description:(NSString *)description previewImageData:(NSData *)data;
		[Static]
		[Export("objectWithURL:title:description:previewImageData:")]
		NSObject ObjectWithURL(NSUrl url, string title, string description, NSData data);

		// +(id)objectWithURL:(NSURL *)url title:(NSString *)title description:(NSString *)description previewImageURL:(NSURL *)previewURL;
		[Static]
		[Export("objectWithURL:title:description:previewImageURL:")]
		NSObject ObjectWithURL(NSUrl url, string title, string description, NSUrl previewURL);
	}

	// @interface QQApiVideoObject : QQApiURLObject
	[BaseType(typeof(QQApiURLObject))]
	interface QQApiVideoObject
	{
		// @property (retain, nonatomic) NSURL * flashURL;
		[Export("flashURL", ArgumentSemantic.Retain)]
		NSUrl FlashURL { get; set; }

		// +(id)objectWithURL:(NSURL *)url title:(NSString *)title description:(NSString *)description previewImageData:(NSData *)data;
		[Static]
		[Export("objectWithURL:title:description:previewImageData:")]
		NSObject ObjectWithURL(NSUrl url, string title, string description, NSData data);

		// +(id)objectWithURL:(NSURL *)url title:(NSString *)title description:(NSString *)description previewImageURL:(NSURL *)previewURL;
		[Static]
		[Export("objectWithURL:title:description:previewImageURL:")]
		NSObject ObjectWithURL(NSUrl url, string title, string description, NSUrl previewURL);
	}

	// @interface QQApiNewsObject : QQApiURLObject
	[BaseType(typeof(QQApiURLObject))]
	interface QQApiNewsObject
	{
		// +(id)objectWithURL:(NSURL *)url title:(NSString *)title description:(NSString *)description previewImageData:(NSData *)data;
		[Static]
		[Export("objectWithURL:title:description:previewImageData:")]
		NSObject ObjectWithURL(NSUrl url, string title, string description, NSData data);

		// +(id)objectWithURL:(NSURL *)url title:(NSString *)title description:(NSString *)description previewImageURL:(NSURL *)previewURL;
		[Static]
		[Export("objectWithURL:title:description:previewImageURL:")]
		NSObject ObjectWithURL(NSUrl url, string title, string description, NSUrl previewURL);
	}

	// @interface QQApiPayObject : QQApiObject
	[BaseType(typeof(QQApiObject))]
	interface QQApiPayObject
	{
		// @property (retain, nonatomic) NSString * OrderNo;
		[Export("OrderNo", ArgumentSemantic.Retain)]
		string OrderNo { get; set; }

		// @property (retain, nonatomic) NSString * AppInfo;
		[Export("AppInfo", ArgumentSemantic.Retain)]
		string AppInfo { get; set; }

		// -(id)initWithOrderNo:(NSString *)OrderNo AppInfo:(NSString *)AppInfo;
		[Export("initWithOrderNo:AppInfo:")]
		IntPtr Constructor(string OrderNo, string AppInfo);

		// +(id)objectWithOrderNo:(NSString *)OrderNo AppInfo:(NSString *)AppInfo;
		[Static]
		[Export("objectWithOrderNo:AppInfo:")]
		NSObject ObjectWithOrderNo(string OrderNo, string AppInfo);
	}

	// @interface QQApiCommonContentObject : QQApiObject
	[BaseType(typeof(QQApiObject))]
	interface QQApiCommonContentObject
	{
		// @property (assign, nonatomic) unsigned int layoutType;
		[Export("layoutType")]
		uint LayoutType { get; set; }

		// @property (assign, nonatomic) NSData * previewImageData;
		[Export("previewImageData", ArgumentSemantic.Assign)]
		NSData PreviewImageData { get; set; }

		// @property (retain, nonatomic) NSArray * textArray;
		[Export("textArray", ArgumentSemantic.Retain)]
		NSObject[] TextArray { get; set; }

		// @property (retain, nonatomic) NSArray * pictureDataArray;
		[Export("pictureDataArray", ArgumentSemantic.Retain)]
		NSObject[] PictureDataArray { get; set; }

		// +(id)objectWithLayoutType:(int)layoutType textArray:(NSArray *)textArray pictureArray:(NSArray *)pictureArray previewImageData:(NSData *)data;
		[Static]
		[Export("objectWithLayoutType:textArray:pictureArray:previewImageData:")]
		NSObject ObjectWithLayoutType(int layoutType, NSObject[] textArray, NSObject[] pictureArray, NSData data);

		// +(id)objectWithDictionary:(NSDictionary *)dic;
		[Static]
		[Export("objectWithDictionary:")]
		NSObject ObjectWithDictionary(NSDictionary dic);

		// -(NSDictionary *)toDictionary;
		[Export("toDictionary")]
		NSDictionary ToDictionary { get; }
	}

	// @interface QQApiAdItem : NSObject
	[BaseType(typeof(NSObject))]
	interface QQApiAdItem
	{
		// @property (retain, nonatomic) NSString * title;
		[Export("title", ArgumentSemantic.Retain)]
		string Title { get; set; }

		// @property (retain, nonatomic) NSString * description;
		[Export("description", ArgumentSemantic.Retain)]
		string Description { get; set; }

		// @property (retain, nonatomic) NSData * imageData;
		[Export("imageData", ArgumentSemantic.Retain)]
		NSData ImageData { get; set; }

		// @property (retain, nonatomic) NSURL * target;
		[Export("target", ArgumentSemantic.Retain)]
		NSUrl Target { get; set; }
	}

	// @interface QQApiWPAObject : QQApiObject
	[BaseType(typeof(QQApiObject))]
	interface QQApiWPAObject
	{
		// @property (retain, nonatomic) NSString * uin;
		[Export("uin", ArgumentSemantic.Retain)]
		string Uin { get; set; }

		// -(id)initWithUin:(NSString *)uin;
		[Export("initWithUin:")]
		IntPtr Constructor(string uin);

		// +(id)objectWithUin:(NSString *)uin;
		[Static]
		[Export("objectWithUin:")]
		NSObject ObjectWithUin(string uin);
	}

	// @interface QQApiAddFriendObject : QQApiObject
	[BaseType(typeof(QQApiObject))]
	interface QQApiAddFriendObject
	{
		// @property (retain, nonatomic) NSString * openID;
		[Export("openID", ArgumentSemantic.Retain)]
		string OpenID { get; set; }

		// @property (retain, nonatomic) NSString * subID;
		[Export("subID", ArgumentSemantic.Retain)]
		string SubID { get; set; }

		// @property (retain, nonatomic) NSString * remark;
		[Export("remark", ArgumentSemantic.Retain)]
		string Remark { get; set; }

		// -(id)initWithOpenID:(NSString *)openID;
		[Export("initWithOpenID:")]
		IntPtr Constructor(string openID);

		// +(id)objecWithOpenID:(NSString *)openID;
		[Static]
		[Export("objecWithOpenID:")]
		NSObject ObjecWithOpenID(string openID);
	}

	// @interface QQApiGameConsortiumBindingGroupObject : QQApiObject
	[BaseType(typeof(QQApiObject))]
	interface QQApiGameConsortiumBindingGroupObject
	{
		// @property (retain, nonatomic) NSString * signature;
		[Export("signature", ArgumentSemantic.Retain)]
		string Signature { get; set; }

		// @property (retain, nonatomic) NSString * unionid;
		[Export("unionid", ArgumentSemantic.Retain)]
		string Unionid { get; set; }

		// @property (retain, nonatomic) NSString * zoneID;
		[Export("zoneID", ArgumentSemantic.Retain)]
		string ZoneID { get; set; }

		// @property (retain, nonatomic) NSString * appDisplayName;
		[Export("appDisplayName", ArgumentSemantic.Retain)]
		string AppDisplayName { get; set; }

		// -(id)initWithGameConsortium:(NSString *)signature unionid:(NSString *)unionid zoneID:(NSString *)zoneID appDisplayName:(NSString *)appDisplayName;
		[Export("initWithGameConsortium:unionid:zoneID:appDisplayName:")]
		IntPtr Constructor(string signature, string unionid, string zoneID, string appDisplayName);

		// +(id)objectWithGameConsortium:(NSString *)signature unionid:(NSString *)unionid zoneID:(NSString *)zoneID appDisplayName:(NSString *)appDisplayName;
		[Static]
		[Export("objectWithGameConsortium:unionid:zoneID:appDisplayName:")]
		NSObject ObjectWithGameConsortium(string signature, string unionid, string zoneID, string appDisplayName);
	}

	// @interface QQApiJoinGroupObject : QQApiObject
	[BaseType(typeof(QQApiObject))]
	interface QQApiJoinGroupObject
	{
		// @property (retain, nonatomic) NSString * groupUin;
		[Export("groupUin", ArgumentSemantic.Retain)]
		string GroupUin { get; set; }

		// @property (retain, nonatomic) NSString * groupKey;
		[Export("groupKey", ArgumentSemantic.Retain)]
		string GroupKey { get; set; }

		// -(id)initWithGroupInfo:(NSString *)groupUin key:(NSString *)groupKey;
		[Export("initWithGroupInfo:key:")]
		IntPtr Constructor(string groupUin, string groupKey);

		// +(id)objectWithGroupInfo:(NSString *)groupUin key:(NSString *)groupKey;
		[Static]
		[Export("objectWithGroupInfo:key:")]
		NSObject ObjectWithGroupInfo(string groupUin, string groupKey);

		// +(id)objectWithGroupKey:(NSString *)groupKey;
		[Static]
		[Export("objectWithGroupKey:")]
		NSObject ObjectWithGroupKey(string groupKey);
	}

	// @interface QQApiGroupChatObject : QQApiObject
	[BaseType(typeof(QQApiObject))]
	interface QQApiGroupChatObject
	{
		// @property (retain, nonatomic) NSString * groupID;
		[Export("groupID", ArgumentSemantic.Retain)]
		string GroupID { get; set; }

		// -(id)initWithGroup:(NSString *)groupID;
		[Export("initWithGroup:")]
		IntPtr Constructor(string groupID);

		// +(id)objectWithGroup:(NSString *)groupID;
		[Static]
		[Export("objectWithGroup:")]
		NSObject ObjectWithGroup(string groupID);
	}

	// @interface QQBaseReq : NSObject
	[BaseType(typeof(NSObject))]
	interface QQBaseReq
	{
		// @property (assign, nonatomic) int type;
		[Export("type")]
		int Type { get; set; }
	}

	// @interface QQBaseResp : NSObject
	[BaseType(typeof(NSObject))]
	interface QQBaseResp
	{
		// @property (copy, nonatomic) NSString * result;
		[Export("result")]
		string Result { get; set; }

		// @property (copy, nonatomic) NSString * errorDescription;
		[Export("errorDescription")]
		string ErrorDescription { get; set; }

		// @property (assign, nonatomic) int type;
		[Export("type")]
		int Type { get; set; }

		// @property (assign, nonatomic) NSString * extendInfo;
		[Export("extendInfo")]
		string ExtendInfo { get; set; }
	}

	// @interface GetMessageFromQQReq : QQBaseReq
	[BaseType(typeof(QQBaseReq))]
	interface GetMessageFromQQReq
	{
		// +(GetMessageFromQQReq *)req;
		[Static]
		[Export("req")]
		GetMessageFromQQReq Req { get; }
	}

	// @interface GetMessageFromQQResp : QQBaseResp
	[BaseType(typeof(QQBaseResp))]
	interface GetMessageFromQQResp
	{
		// +(GetMessageFromQQResp *)respWithContent:(QQApiObject *)message;
		[Static]
		[Export("respWithContent:")]
		GetMessageFromQQResp RespWithContent(QQApiObject message);

		// @property (retain, nonatomic) QQApiObject * message;
		[Export("message", ArgumentSemantic.Retain)]
		QQApiObject Message { get; set; }
	}

	// @interface SendMessageToQQReq : QQBaseReq
	[BaseType(typeof(QQBaseReq))]
	interface SendMessageToQQReq
	{
		// +(SendMessageToQQReq *)reqWithContent:(QQApiObject *)message;
		[Static]
		[Export("reqWithContent:")]
		SendMessageToQQReq ReqWithContent(QQApiObject message);

		// @property (retain, nonatomic) QQApiObject * message;
		[Export("message", ArgumentSemantic.Retain)]
		QQApiObject Message { get; set; }
	}

	// @interface SendMessageToQQResp : QQBaseResp
	[BaseType(typeof(QQBaseResp))]
	interface SendMessageToQQResp
	{
		// +(SendMessageToQQResp *)respWithResult:(NSString *)result errorDescription:(NSString *)errDesp extendInfo:(NSString *)extendInfo;
		[Static]
		[Export("respWithResult:errorDescription:extendInfo:")]
		SendMessageToQQResp RespWithResult(string result, string errDesp, string extendInfo);
	}

	// @interface ShowMessageFromQQReq : QQBaseReq
	[BaseType(typeof(QQBaseReq))]
	interface ShowMessageFromQQReq
	{
		// +(ShowMessageFromQQReq *)reqWithContent:(QQApiObject *)message;
		[Static]
		[Export("reqWithContent:")]
		ShowMessageFromQQReq ReqWithContent(QQApiObject message);

		// @property (retain, nonatomic) QQApiObject * message;
		[Export("message", ArgumentSemantic.Retain)]
		QQApiObject Message { get; set; }
	}

	// @interface ShowMessageFromQQResp : QQBaseResp
	[BaseType(typeof(QQBaseResp))]
	interface ShowMessageFromQQResp
	{
		// +(ShowMessageFromQQResp *)respWithResult:(NSString *)result errorDescription:(NSString *)errDesp;
		[Static]
		[Export("respWithResult:errorDescription:")]
		ShowMessageFromQQResp RespWithResult(string result, string errDesp);
	}

	// @protocol QQApiInterfaceDelegate <NSObject>
	[Protocol, Model]
	[BaseType(typeof(NSObject))]
	interface QQApiInterfaceDelegate
	{
		// @required -(void)onReq:(QQBaseReq *)req;
		[Abstract]
		[Export("onReq:")]
		void OnReq(QQBaseReq req);

		// @required -(void)onResp:(QQBaseResp *)resp;
		[Abstract]
		[Export("onResp:")]
		void OnResp(QQBaseResp resp);

		// @required -(void)isOnlineResponse:(NSDictionary *)response;
		[Abstract]
		[Export("isOnlineResponse:")]
		void IsOnlineResponse(NSDictionary response);
	}

	// @interface QQApiInterface : NSObject
	[BaseType(typeof(NSObject))]
	interface QQApiInterface
	{
		// +(BOOL)handleOpenURL:(NSURL *)url delegate:(id<QQApiInterfaceDelegate>)delegate;
		[Static]
		[Export("handleOpenURL:delegate:")]
		bool HandleOpenURL(NSUrl url, QQApiInterfaceDelegate @delegate);

		// +(QQApiSendResultCode)sendReq:(QQBaseReq *)req;
		[Static]
		[Export("sendReq:")]
		QQApiSendResultCode SendReq(QQBaseReq req);

		// +(QQApiSendResultCode)SendReqToQZone:(QQBaseReq *)req;
		[Static]
		[Export("SendReqToQZone:")]
		QQApiSendResultCode SendReqToQZone(QQBaseReq req);

		// +(QQApiSendResultCode)SendReqToQQGroupTribe:(QQBaseReq *)req;
		[Static]
		[Export("SendReqToQQGroupTribe:")]
		QQApiSendResultCode SendReqToQQGroupTribe(QQBaseReq req);

		// +(QQApiSendResultCode)sendResp:(QQBaseResp *)resp;
		[Static]
		[Export("sendResp:")]
		QQApiSendResultCode SendResp(QQBaseResp resp);

		// +(BOOL)isQQInstalled;
		[Static]
		[Export("isQQInstalled")]
		bool IsQQInstalled { get; }

		// +(void)getQQUinOnlineStatues:(NSArray *)QQUins delegate:(id<QQApiInterfaceDelegate>)delegate;
		[Static]
		[Export("getQQUinOnlineStatues:delegate:")]
		void GetQQUinOnlineStatues(NSObject[] QQUins, QQApiInterfaceDelegate @delegate);

		// +(BOOL)isQQSupportApi;
		[Static]
		[Export("isQQSupportApi")]
		bool IsQQSupportApi { get; }

		// +(BOOL)openQQ;
		[Static]
		[Export("openQQ")]
		bool OpenQQ { get; }

		// +(NSString *)getQQInstallUrl;
		[Static]
		[Export("getQQInstallUrl")]
		string QQInstallUrl { get; }
	}

	// @interface APIResponse : NSObject <NSCoding>
	[BaseType(typeof(NSObject))]
	interface APIResponse : INSCoding
	{
		// @property (assign, nonatomic) int detailRetCode;
		[Export("detailRetCode")]
		int DetailRetCode { get; set; }

		// @property (assign, nonatomic) int retCode;
		[Export("retCode")]
		int RetCode { get; set; }

		// @property (assign, nonatomic) int seq;
		[Export("seq")]
		int Seq { get; set; }

		// @property (retain, nonatomic) NSString * errorMsg;
		[Export("errorMsg", ArgumentSemantic.Retain)]
		string ErrorMsg { get; set; }

		// @property (retain, nonatomic) NSDictionary * jsonResponse;
		[Export("jsonResponse", ArgumentSemantic.Retain)]
		NSDictionary JsonResponse { get; set; }

		// @property (retain, nonatomic) NSString * message;
		[Export("message", ArgumentSemantic.Retain)]
		string Message { get; set; }

		// @property (retain, nonatomic) id userData;
		[Export("userData", ArgumentSemantic.Retain)]
		NSObject UserData { get; set; }
	}

	partial interface Constants
	{
		// extern NSString *const PARAM_USER_DATA;
		[Field("PARAM_USER_DATA", "__Internal")]
		NSString PARAM_USER_DATA { get; }

		// extern NSString *const PARAM_APP_ICON;
		[Field("PARAM_APP_ICON", "__Internal")]
		NSString PARAM_APP_ICON { get; }

		// extern NSString *const PARAM_APP_DESC;
		[Field("PARAM_APP_DESC", "__Internal")]
		NSString PARAM_APP_DESC { get; }

		// extern NSString *const PARAM_APP_INVITED_OPENIDS;
		[Field("PARAM_APP_INVITED_OPENIDS", "__Internal")]
		NSString PARAM_APP_INVITED_OPENIDS { get; }

		// extern NSString *const PARAM_SENDSTORY_RECEIVER;
		[Field("PARAM_SENDSTORY_RECEIVER", "__Internal")]
		NSString PARAM_SENDSTORY_RECEIVER { get; }

		// extern NSString *const PARAM_SENDSTORY_TITLE;
		[Field("PARAM_SENDSTORY_TITLE", "__Internal")]
		NSString PARAM_SENDSTORY_TITLE { get; }

		// extern NSString *const PARAM_SENDSTORY_COMMENT;
		[Field("PARAM_SENDSTORY_COMMENT", "__Internal")]
		NSString PARAM_SENDSTORY_COMMENT { get; }

		// extern NSString *const PARAM_SENDSTORY_SUMMARY;
		[Field("PARAM_SENDSTORY_SUMMARY", "__Internal")]
		NSString PARAM_SENDSTORY_SUMMARY { get; }

		// extern NSString *const PARAM_SENDSTORY_IMAGE;
		[Field("PARAM_SENDSTORY_IMAGE", "__Internal")]
		NSString PARAM_SENDSTORY_IMAGE { get; }

		// extern NSString *const PARAM_SENDSTORY_URL;
		[Field("PARAM_SENDSTORY_URL", "__Internal")]
		NSString PARAM_SENDSTORY_URL { get; }

		// extern NSString *const PARAM_SENDSTORY_ACT;
		[Field("PARAM_SENDSTORY_ACT", "__Internal")]
		NSString PARAM_SENDSTORY_ACT { get; }

		// extern NSString *const PARAM_SETUSERHEAD_PIC;
		[Field("PARAM_SETUSERHEAD_PIC", "__Internal")]
		NSString PARAM_SETUSERHEAD_PIC { get; }

		// extern NSString *const PARAM_SETUSERHEAD_FILENAME;
		[Field("PARAM_SETUSERHEAD_FILENAME", "__Internal")]
		NSString PARAM_SETUSERHEAD_FILENAME { get; }

		// extern NSString *const PARAM_RETCODE;
		[Field("PARAM_RETCODE", "__Internal")]
		NSString PARAM_RETCODE { get; }

		// extern NSString *const PARAM_MESSAGE;
		[Field("PARAM_MESSAGE", "__Internal")]
		NSString PARAM_MESSAGE { get; }

		// extern NSString *const PARAM_DATA;
		[Field("PARAM_DATA", "__Internal")]
		NSString PARAM_DATA { get; }

		// extern NSString *const TCOpenSDKErrorKeyExtraInfo;
		[Field("TCOpenSDKErrorKeyExtraInfo", "__Internal")]
		NSString TCOpenSDKErrorKeyExtraInfo { get; }

		// extern NSString *const TCOpenSDKErrorKeyRetCode;
		[Field("TCOpenSDKErrorKeyRetCode", "__Internal")]
		NSString TCOpenSDKErrorKeyRetCode { get; }

		// extern NSString *const TCOpenSDKErrorKeyMsg;
		[Field("TCOpenSDKErrorKeyMsg", "__Internal")]
		NSString TCOpenSDKErrorKeyMsg { get; }

		// extern NSString *const TCOpenSDKErrorMsgUnsupportedAPI;
		[Field("TCOpenSDKErrorMsgUnsupportedAPI", "__Internal")]
		NSString TCOpenSDKErrorMsgUnsupportedAPI { get; }

		// extern NSString *const TCOpenSDKErrorMsgSuccess;
		[Field("TCOpenSDKErrorMsgSuccess", "__Internal")]
		NSString TCOpenSDKErrorMsgSuccess { get; }

		// extern NSString *const TCOpenSDKErrorMsgUnknown;
		[Field("TCOpenSDKErrorMsgUnknown", "__Internal")]
		NSString TCOpenSDKErrorMsgUnknown { get; }

		// extern NSString *const TCOpenSDKErrorMsgUserCancel;
		[Field("TCOpenSDKErrorMsgUserCancel", "__Internal")]
		NSString TCOpenSDKErrorMsgUserCancel { get; }

		// extern NSString *const TCOpenSDKErrorMsgReLogin;
		[Field("TCOpenSDKErrorMsgReLogin", "__Internal")]
		NSString TCOpenSDKErrorMsgReLogin { get; }

		// extern NSString *const TCOpenSDKErrorMsgOperationDeny;
		[Field("TCOpenSDKErrorMsgOperationDeny", "__Internal")]
		NSString TCOpenSDKErrorMsgOperationDeny { get; }

		// extern NSString *const TCOpenSDKErrorMsgNetwork;
		[Field("TCOpenSDKErrorMsgNetwork", "__Internal")]
		NSString TCOpenSDKErrorMsgNetwork { get; }

		// extern NSString *const TCOpenSDKErrorMsgURL;
		[Field("TCOpenSDKErrorMsgURL", "__Internal")]
		NSString TCOpenSDKErrorMsgURL { get; }

		// extern NSString *const TCOpenSDKErrorMsgDataParse;
		[Field("TCOpenSDKErrorMsgDataParse", "__Internal")]
		NSString TCOpenSDKErrorMsgDataParse { get; }

		// extern NSString *const TCOpenSDKErrorMsgParam;
		[Field("TCOpenSDKErrorMsgParam", "__Internal")]
		NSString TCOpenSDKErrorMsgParam { get; }

		// extern NSString *const TCOpenSDKErrorMsgTimeout;
		[Field("TCOpenSDKErrorMsgTimeout", "__Internal")]
		NSString TCOpenSDKErrorMsgTimeout { get; }

		// extern NSString *const TCOpenSDKErrorMsgSecurity;
		[Field("TCOpenSDKErrorMsgSecurity", "__Internal")]
		NSString TCOpenSDKErrorMsgSecurity { get; }

		// extern NSString *const TCOpenSDKErrorMsgIO;
		[Field("TCOpenSDKErrorMsgIO", "__Internal")]
		NSString TCOpenSDKErrorMsgIO { get; }

		// extern NSString *const TCOpenSDKErrorMsgServer;
		[Field("TCOpenSDKErrorMsgServer", "__Internal")]
		NSString TCOpenSDKErrorMsgServer { get; }

		// extern NSString *const TCOpenSDKErrorMsgWebPage;
		[Field("TCOpenSDKErrorMsgWebPage", "__Internal")]
		NSString TCOpenSDKErrorMsgWebPage { get; }

		// extern NSString *const TCOpenSDKErrorMsgUserHeadPicLarge;
		[Field("TCOpenSDKErrorMsgUserHeadPicLarge", "__Internal")]
		NSString TCOpenSDKErrorMsgUserHeadPicLarge { get; }
	}

	[Static]
	partial interface Constants
	{
		// extern NSString *const kOPEN_PERMISSION_ADD_TOPIC;
		[Field("kOPEN_PERMISSION_ADD_TOPIC", "__Internal")]
		NSString kOPEN_PERMISSION_ADD_TOPIC { get; }

		// extern NSString *const kOPEN_PERMISSION_ADD_ONE_BLOG;
		[Field("kOPEN_PERMISSION_ADD_ONE_BLOG", "__Internal")]
		NSString kOPEN_PERMISSION_ADD_ONE_BLOG { get; }

		// extern NSString *const kOPEN_PERMISSION_ADD_ALBUM;
		[Field("kOPEN_PERMISSION_ADD_ALBUM", "__Internal")]
		NSString kOPEN_PERMISSION_ADD_ALBUM { get; }

		// extern NSString *const kOPEN_PERMISSION_UPLOAD_PIC;
		[Field("kOPEN_PERMISSION_UPLOAD_PIC", "__Internal")]
		NSString kOPEN_PERMISSION_UPLOAD_PIC { get; }

		// extern NSString *const kOPEN_PERMISSION_LIST_ALBUM;
		[Field("kOPEN_PERMISSION_LIST_ALBUM", "__Internal")]
		NSString kOPEN_PERMISSION_LIST_ALBUM { get; }

		// extern NSString *const kOPEN_PERMISSION_ADD_SHARE;
		[Field("kOPEN_PERMISSION_ADD_SHARE", "__Internal")]
		NSString kOPEN_PERMISSION_ADD_SHARE { get; }

		// extern NSString *const kOPEN_PERMISSION_CHECK_PAGE_FANS;
		[Field("kOPEN_PERMISSION_CHECK_PAGE_FANS", "__Internal")]
		NSString kOPEN_PERMISSION_CHECK_PAGE_FANS { get; }

		// extern NSString *const kOPEN_PERMISSION_GET_INFO;
		[Field("kOPEN_PERMISSION_GET_INFO", "__Internal")]
		NSString kOPEN_PERMISSION_GET_INFO { get; }

		// extern NSString *const kOPEN_PERMISSION_GET_OTHER_INFO;
		[Field("kOPEN_PERMISSION_GET_OTHER_INFO", "__Internal")]
		NSString kOPEN_PERMISSION_GET_OTHER_INFO { get; }

		// extern NSString *const kOPEN_PERMISSION_GET_VIP_INFO;
		[Field("kOPEN_PERMISSION_GET_VIP_INFO", "__Internal")]
		NSString kOPEN_PERMISSION_GET_VIP_INFO { get; }

		// extern NSString *const kOPEN_PERMISSION_GET_VIP_RICH_INFO;
		[Field("kOPEN_PERMISSION_GET_VIP_RICH_INFO", "__Internal")]
		NSString kOPEN_PERMISSION_GET_VIP_RICH_INFO { get; }

		// extern NSString *const kOPEN_PERMISSION_GET_USER_INFO;
		[Field("kOPEN_PERMISSION_GET_USER_INFO", "__Internal")]
		NSString kOPEN_PERMISSION_GET_USER_INFO { get; }

		// extern NSString *const kOPEN_PERMISSION_GET_SIMPLE_USER_INFO;
		[Field("kOPEN_PERMISSION_GET_SIMPLE_USER_INFO", "__Internal")]
		NSString kOPEN_PERMISSION_GET_SIMPLE_USER_INFO { get; }
	}

	// @interface TCAPIRequest : NSMutableDictionary
	[BaseType(typeof(NSMutableDictionary))]
	interface TCAPIRequest
	{
		// @property (readonly, nonatomic) NSURL * apiURL;
		[Export("apiURL")]
		NSUrl ApiURL { get; }

		// @property (readonly, nonatomic) NSString * method;
		[Export("method")]
		string Method { get; }

		// @property (retain, nonatomic) TCRequiredId paramUserData;
		[Export("paramUserData", ArgumentSemantic.Retain)]
		NSObject ParamUserData { get; set; }

		// @property (readonly, nonatomic) APIResponse * response;
		[Export("response")]
		APIResponse Response { get; }

		// -(void)cancel;
		[Export("cancel")]
		void Cancel();
	}

	// @protocol TCAPIRequestDelegate <NSObject>
	[Protocol, Model]
	[BaseType(typeof(NSObject))]
	interface TCAPIRequestDelegate
	{
		// @optional -(void)cgiRequest:(TCAPIRequest *)request didResponse:(APIResponse *)response;
		[Export("cgiRequest:didResponse:")]
		void DidResponse(TCAPIRequest request, APIResponse response);
	}

	// @interface TencentApiReq : NSObject <NSCoding>
	[BaseType(typeof(NSObject))]
	interface TencentApiReq : INSCoding
	{
		// +(TencentApiReq *)reqFromSeq:(NSInteger)apiSeq type:(TencentReqMessageType)type;
		[Static]
		[Export("reqFromSeq:type:")]
		TencentApiReq ReqFromSeq(nint apiSeq, TencentReqMessageType type);

		// @property (readonly, assign, nonatomic) TCRequiredInt nMessageType;
		[Export("nMessageType")]
		nint NMessageType { get; }

		// @property (readonly, assign, nonatomic) NSInteger nPlatform;
		[Export("nPlatform")]
		nint NPlatform { get; }

		// @property (readonly, assign, nonatomic) NSInteger nSdkVersion;
		[Export("nSdkVersion")]
		nint NSdkVersion { get; }

		// @property (readonly, assign, nonatomic) TCRequiredInt nSeq;
		[Export("nSeq")]
		nint NSeq { get; }

		// @property (retain, nonatomic) TCRequiredStr sAppID;
		[Export("sAppID", ArgumentSemantic.Retain)]
		string SAppID { get; set; }

		// @property (retain, nonatomic) TCOptionalArray arrMessage;
		[Export("arrMessage", ArgumentSemantic.Retain)]
		NSObject[] ArrMessage { get; set; }

		// @property (retain, nonatomic) TCOptionalStr sDescription;
		[Export("sDescription", ArgumentSemantic.Retain)]
		string SDescription { get; set; }
	}

	// @interface TencentApiResp : NSObject <NSCoding>
	[BaseType(typeof(NSObject))]
	interface TencentApiResp : INSCoding
	{
		// +(TencentApiResp *)respFromReq:(TencentApiReq *)req;
		[Static]
		[Export("respFromReq:")]
		TencentApiResp RespFromReq(TencentApiReq req);

		// @property (assign, nonatomic) TCOptionalInt nRetCode;
		[Export("nRetCode")]
		nint NRetCode { get; set; }

		// @property (retain, nonatomic) TCOptionalStr sRetMsg;
		[Export("sRetMsg", ArgumentSemantic.Retain)]
		string SRetMsg { get; set; }

		// @property (retain, nonatomic) TCOptionalReq objReq;
		[Export("objReq", ArgumentSemantic.Retain)]
		TencentApiReq ObjReq { get; set; }
	}

	// @interface TencentBaseMessageObj : NSObject <NSCoding>
	[BaseType(typeof(NSObject))]
	interface TencentBaseMessageObj : INSCoding
	{
		// @property (assign, nonatomic) NSInteger nVersion;
		[Export("nVersion")]
		nint NVersion { get; set; }

		// @property (retain, nonatomic) NSString * sName;
		[Export("sName", ArgumentSemantic.Retain)]
		string SName { get; set; }

		// @property (retain, nonatomic) NSDictionary * dictExpandInfo;
		[Export("dictExpandInfo", ArgumentSemantic.Retain)]
		NSDictionary DictExpandInfo { get; set; }

		// -(BOOL)isVaild;
		[Export("isVaild")]
		bool IsVaild { get; }
	}

	// @interface TencentTextMessageObjV1 : TencentBaseMessageObj
	[BaseType(typeof(TencentBaseMessageObj))]
	interface TencentTextMessageObjV1
	{
		// @property (retain, nonatomic) NSString * sText;
		[Export("sText", ArgumentSemantic.Retain)]
		string SText { get; set; }

		// -(id)initWithText:(NSString *)text;
		[Export("initWithText:")]
		IntPtr Constructor(string text);
	}

	// @interface TencentImageMessageObjV1 : TencentBaseMessageObj
	[BaseType(typeof(TencentBaseMessageObj))]
	interface TencentImageMessageObjV1
	{
		// @property (retain, nonatomic) NSData * dataImage;
		[Export("dataImage", ArgumentSemantic.Retain)]
		NSData DataImage { get; set; }

		// @property (retain, nonatomic) NSData * dataThumbImage;
		[Export("dataThumbImage", ArgumentSemantic.Retain)]
		NSData DataThumbImage { get; set; }

		// @property (retain, nonatomic) NSString * sUrl;
		[Export("sUrl", ArgumentSemantic.Retain)]
		string SUrl { get; set; }

		// @property (retain, nonatomic) NSString * sDescription;
		[Export("sDescription", ArgumentSemantic.Retain)]
		string SDescription { get; set; }

		// @property (assign, nonatomic) CGSize szImage;
		[Export("szImage", ArgumentSemantic.Assign)]
		CGSize SzImage { get; set; }

		// @property (readonly, assign) NSInteger nType;
		[Export("nType")]
		nint NType { get; }

		// -(id)initWithImageData:(NSData *)dataImage;
		[Export("initWithImageData:")]
		IntPtr Constructor(NSData dataImage);

		// -(id)initWithImageUrl:(NSString *)url;
		[Export("initWithImageUrl:")]
		IntPtr Constructor(string url);

		// -(id)initWithType:(TencentApiImageSourceType)type;
		[Export("initWithType:")]
		IntPtr Constructor(TencentApiImageSourceType type);
	}

	// @interface TencentAudioMessageObjV1 : TencentBaseMessageObj
	[BaseType(typeof(TencentBaseMessageObj))]
	interface TencentAudioMessageObjV1
	{
		// @property (retain, nonatomic) NSString * sUrl;
		[Export("sUrl", ArgumentSemantic.Retain)]
		string SUrl { get; set; }

		// @property (retain, nonatomic) NSData * dataImagePreview;
		[Export("dataImagePreview", ArgumentSemantic.Retain)]
		NSData DataImagePreview { get; set; }

		// @property (retain, nonatomic) NSString * sImagePreviewUrl;
		[Export("sImagePreviewUrl", ArgumentSemantic.Retain)]
		string SImagePreviewUrl { get; set; }

		// @property (retain, nonatomic) NSString * sDescription;
		[Export("sDescription", ArgumentSemantic.Retain)]
		string SDescription { get; set; }

		// -(id)initWithAudioUrl:(NSString *)url;
		[Export("initWithAudioUrl:")]
		IntPtr Constructor(string url);
	}

	// @interface TencentVideoMessageV1 : TencentBaseMessageObj
	[BaseType(typeof(TencentBaseMessageObj))]
	interface TencentVideoMessageV1
	{
		// @property (retain, nonatomic) NSString * sUrl;
		[Export("sUrl", ArgumentSemantic.Retain)]
		string SUrl { get; set; }

		// @property (readonly, assign, nonatomic) NSInteger nType;
		[Export("nType")]
		nint NType { get; }

		// @property (retain, nonatomic) NSData * dataImagePreview;
		[Export("dataImagePreview", ArgumentSemantic.Retain)]
		NSData DataImagePreview { get; set; }

		// @property (retain, nonatomic) NSString * sImagePreviewUrl;
		[Export("sImagePreviewUrl", ArgumentSemantic.Retain)]
		string SImagePreviewUrl { get; set; }

		// @property (retain, nonatomic) NSString * sDescription;
		[Export("sDescription", ArgumentSemantic.Retain)]
		string SDescription { get; set; }

		// -(id)initWithVideoUrl:(NSString *)url type:(TencentApiVideoSourceType)type;
		[Export("initWithVideoUrl:type:")]
		IntPtr Constructor(string url, TencentApiVideoSourceType type);

		// -(id)initWithType:(TencentApiVideoSourceType)type;
		[Export("initWithType:")]
		IntPtr Constructor(TencentApiVideoSourceType type);
	}

	// @interface TencentImageAndVideoMessageObjV1 : TencentBaseMessageObj
	[BaseType(typeof(TencentBaseMessageObj))]
	interface TencentImageAndVideoMessageObjV1
	{
		// @property (retain, nonatomic) TencentImageMessageObjV1 * objImageMessage;
		[Export("objImageMessage", ArgumentSemantic.Retain)]
		TencentImageMessageObjV1 ObjImageMessage { get; set; }

		// @property (retain, nonatomic) TencentVideoMessageV1 * objVideoMessage;
		[Export("objVideoMessage", ArgumentSemantic.Retain)]
		TencentVideoMessageV1 ObjVideoMessage { get; set; }

		// -(id)initWithMessage:(NSData *)dataImage videoUrl:(NSString *)url;
		[Export("initWithMessage:videoUrl:")]
		IntPtr Constructor(NSData dataImage, string url);

		// -(void)setDataImage:(NSData *)dataImage;
		[Export("setDataImage:")]
		void SetDataImage(NSData dataImage);

		// -(void)setVideoUrl:(NSString *)videoUrl;
		[Export("setVideoUrl:")]
		void SetVideoUrl(string videoUrl);
	}

	// @protocol TencentApiInterfaceDelegate <NSObject>
	[BaseType(typeof(NSObject))]
	[Model]
	interface TencentApiInterfaceDelegate
	{
		// @optional -(BOOL)onTencentReq:(TencentApiReq *)req;
		[Export("onTencentReq:")]
		bool OnTencentReq(TencentApiReq req);

		// @optional -(BOOL)onTencentResp:(TencentApiResp *)resp;
		[Export("onTencentResp:")]
		bool OnTencentResp(TencentApiResp resp);
	}

	// @interface TencentApiInterface : NSObject
	[BaseType(typeof(NSObject))]
	interface TencentApiInterface
	{
		// +(TencentApiRetCode)sendRespMessageToTencentApp:(TencentApiResp *)resp;
		[Static]
		[Export("sendRespMessageToTencentApp:")]
		TencentApiRetCode SendRespMessageToTencentApp(TencentApiResp resp);

		// +(BOOL)canOpenURL:(NSURL *)url delegate:(id<TencentApiInterfaceDelegate>)delegate;
		[Static]
		[Export("canOpenURL:delegate:")]
		bool CanOpenURL(NSUrl url, TencentApiInterfaceDelegate @delegate);

		// +(BOOL)handleOpenURL:(NSURL *)url delegate:(id<TencentApiInterfaceDelegate>)delegate;
		[Static]
		[Export("handleOpenURL:delegate:")]
		bool HandleOpenURL(NSUrl url, TencentApiInterfaceDelegate @delegate);

		// +(BOOL)isTencentAppInstall:(TecnentPlatformType)platform;
		[Static]
		[Export("isTencentAppInstall:")]
		bool IsTencentAppInstall(TecnentPlatformType platform);

		// +(BOOL)isTencentAppSupportTencentApi:(TecnentPlatformType)platform;
		[Static]
		[Export("isTencentAppSupportTencentApi:")]
		bool IsTencentAppSupportTencentApi(TecnentPlatformType platform);
	}

	// @interface TCAddTopicDic : TCAPIRequest
	[BaseType(typeof(TCAPIRequest))]
	interface TCAddTopicDic
	{
		// +(TCAddTopicDic *)dictionary;
		[Static]
		[Export("dictionary")]
		
		TCAddTopicDic Dictionary { get; }

		// @property (retain, nonatomic) TCOptionalStr paramRichtype;
		[Export("paramRichtype", ArgumentSemantic.Retain)]
		string ParamRichtype { get; set; }

		// @property (retain, nonatomic) TCOptionalStr paramRichval;
		[Export("paramRichval", ArgumentSemantic.Retain)]
		string ParamRichval { get; set; }

		// @property (retain, nonatomic) TCRequiredStr paramCon;
		[Export("paramCon", ArgumentSemantic.Retain)]
		string ParamCon { get; set; }

		// @property (retain, nonatomic) TCOptionalStr paramLbs_nm;
		[Export("paramLbs_nm", ArgumentSemantic.Retain)]
		string ParamLbs_nm { get; set; }

		// @property (retain, nonatomic) TCOptionalStr paramLbs_x;
		[Export("paramLbs_x", ArgumentSemantic.Retain)]
		string ParamLbs_x { get; set; }

		// @property (retain, nonatomic) TCOptionalStr paramLbs_y;
		[Export("paramLbs_y", ArgumentSemantic.Retain)]
		string ParamLbs_y { get; set; }

		// @property (retain, nonatomic) TCOptionalStr paramThirdSource;
		[Export("paramThirdSource", ArgumentSemantic.Retain)]
		string ParamThirdSource { get; set; }
	}

	// @interface TCAddOneBlogDic : TCAPIRequest
	[BaseType(typeof(TCAPIRequest))]
	interface TCAddOneBlogDic
	{
		// +(TCAddOneBlogDic *)dictionary;
		[Static]
		[Export("dictionary")]
		
		TCAddOneBlogDic Dictionary { get; }

		// @property (retain, nonatomic) TCRequiredStr paramTitle;
		[Export("paramTitle", ArgumentSemantic.Retain)]
		string ParamTitle { get; set; }

		// @property (retain, nonatomic) TCRequiredStr paramContent;
		[Export("paramContent", ArgumentSemantic.Retain)]
		string ParamContent { get; set; }
	}

	// @interface TCAddAlbumDic : TCAPIRequest
	[BaseType(typeof(TCAPIRequest))]
	interface TCAddAlbumDic
	{
		// +(TCAddAlbumDic *)dictionary;
		[Static]
		[Export("dictionary")]
		
		TCAddAlbumDic Dictionary { get; }

		// @property (retain, nonatomic) TCRequiredStr paramAlbumname;
		[Export("paramAlbumname", ArgumentSemantic.Retain)]
		string ParamAlbumname { get; set; }

		// @property (retain, nonatomic) TCOptionalStr paramAlbumdesc;
		[Export("paramAlbumdesc", ArgumentSemantic.Retain)]
		string ParamAlbumdesc { get; set; }

		// @property (retain, nonatomic) TCOptionalStr paramPriv;
		[Export("paramPriv", ArgumentSemantic.Retain)]
		string ParamPriv { get; set; }

		// @property (retain, nonatomic) TCOptionalStr paramQuestion;
		[Export("paramQuestion", ArgumentSemantic.Retain)]
		string ParamQuestion { get; set; }

		// @property (retain, nonatomic) TCOptionalStr paramAnswer;
		[Export("paramAnswer", ArgumentSemantic.Retain)]
		string ParamAnswer { get; set; }
	}

	// @interface TCUploadPicDic : TCAPIRequest
	[BaseType(typeof(TCAPIRequest))]
	interface TCUploadPicDic
	{
		// +(TCUploadPicDic *)dictionary;
		[Static]
		[Export("dictionary")]
		
		TCUploadPicDic Dictionary { get; }

		// @property (retain, nonatomic) TCOptionalStr paramPhotodesc;
		[Export("paramPhotodesc", ArgumentSemantic.Retain)]
		string ParamPhotodesc { get; set; }

		// @property (retain, nonatomic) TCOptionalStr paramTitle;
		[Export("paramTitle", ArgumentSemantic.Retain)]
		string ParamTitle { get; set; }

		// @property (retain, nonatomic) TCOptionalStr paramAlbumid;
		[Export("paramAlbumid", ArgumentSemantic.Retain)]
		string ParamAlbumid { get; set; }

		// @property (assign, nonatomic) TCOptionalStr paramMobile;
		[Export("paramMobile")]
		string ParamMobile { get; set; }

		// @property (retain, nonatomic) TCOptionalStr paramX;
		[Export("paramX", ArgumentSemantic.Retain)]
		string ParamX { get; set; }

		// @property (retain, nonatomic) TCOptionalStr paramY;
		[Export("paramY", ArgumentSemantic.Retain)]
		string ParamY { get; set; }

		// @property (retain, nonatomic) TCRequiredImage paramPicture;
		[Export("paramPicture", ArgumentSemantic.Retain)]
		UIImage ParamPicture { get; set; }

		// @property (assign, nonatomic) TCOptionalStr paramNeedfeed;
		[Export("paramNeedfeed")]
		string ParamNeedfeed { get; set; }

		// @property (assign, nonatomic) TCOptionalStr paramSuccessnum;
		[Export("paramSuccessnum")]
		string ParamSuccessnum { get; set; }

		// @property (assign, nonatomic) TCOptionalStr paramPicnum;
		[Export("paramPicnum")]
		string ParamPicnum { get; set; }
	}

	// @interface TCAddShareDic : TCAPIRequest
	[BaseType(typeof(TCAPIRequest))]
	interface TCAddShareDic
	{
		// +(TCAddShareDic *)dictionary;
		[Static]
		[Export("dictionary")]
		
		TCAddShareDic Dictionary { get; }

		// @property (retain, nonatomic) TCRequiredStr paramTitle;
		[Export("paramTitle", ArgumentSemantic.Retain)]
		string ParamTitle { get; set; }

		// @property (retain, nonatomic) TCRequiredStr paramUrl;
		[Export("paramUrl", ArgumentSemantic.Retain)]
		string ParamUrl { get; set; }

		// @property (retain, nonatomic) TCOptionalStr paramComment;
		[Export("paramComment", ArgumentSemantic.Retain)]
		string ParamComment { get; set; }

		// @property (retain, nonatomic) TCOptionalStr paramSummary;
		[Export("paramSummary", ArgumentSemantic.Retain)]
		string ParamSummary { get; set; }

		// @property (retain, nonatomic) TCOptionalStr paramImages;
		[Export("paramImages", ArgumentSemantic.Retain)]
		string ParamImages { get; set; }

		// @property (retain, nonatomic) TCOptionalStr paramType;
		[Export("paramType", ArgumentSemantic.Retain)]
		string ParamType { get; set; }

		// @property (retain, nonatomic) TCOptionalStr paramPlayurl;
		[Export("paramPlayurl", ArgumentSemantic.Retain)]
		string ParamPlayurl { get; set; }

		// @property (retain, nonatomic) TCRequiredStr paramSite;
		[Export("paramSite", ArgumentSemantic.Retain)]
		string ParamSite { get; set; }

		// @property (retain, nonatomic) TCRequiredStr paramFromurl;
		[Export("paramFromurl", ArgumentSemantic.Retain)]
		string ParamFromurl { get; set; }

		// @property (retain, nonatomic) TCOptionalStr paramNswb;
		[Export("paramNswb", ArgumentSemantic.Retain)]
		string ParamNswb { get; set; }
	}

	// @interface TCCheckPageFansDic : TCAPIRequest
	[BaseType(typeof(TCAPIRequest))]
	interface TCCheckPageFansDic
	{
		// +(TCCheckPageFansDic *)dictionary;
		[Static]
		[Export("dictionary")]
		
		TCCheckPageFansDic Dictionary { get; }

		// @property (retain, nonatomic) TCRequiredStr paramPage_id;
		[Export("paramPage_id", ArgumentSemantic.Retain)]
		string ParamPage_id { get; set; }
	}

	// @interface TCSetUserHeadpic : TCAPIRequest
	[BaseType(typeof(TCAPIRequest))]
	interface TCSetUserHeadpic
	{
		// +(TCSetUserHeadpic *)dictionary;
		[Static]
		[Export("dictionary")]
		
		TCSetUserHeadpic Dictionary { get; }

		// @property (retain, nonatomic) TCRequiredImage paramImage;
		[Export("paramImage", ArgumentSemantic.Retain)]
		UIImage ParamImage { get; set; }

		// @property (retain, nonatomic) TCOptionalStr paramFileName;
		[Export("paramFileName", ArgumentSemantic.Retain)]
		string ParamFileName { get; set; }
	}

	// @interface TCListPhotoDic : TCAPIRequest
	[BaseType(typeof(TCAPIRequest))]
	interface TCListPhotoDic
	{
		// +(TCListPhotoDic *)dictionary;
		[Static]
		[Export("dictionary")]
		
		TCListPhotoDic Dictionary { get; }

		// @property (retain, nonatomic) TCRequiredStr paramAlbumid;
		[Export("paramAlbumid", ArgumentSemantic.Retain)]
		string ParamAlbumid { get; set; }
	}

	// @interface TCSendStoryDic : TCAPIRequest
	[BaseType(typeof(TCAPIRequest))]
	interface TCSendStoryDic
	{
		// +(TCSendStoryDic *)dictionary;
		[Static]
		[Export("dictionary")]
		
		TCSendStoryDic Dictionary { get; }

		// @property (retain, nonatomic) TCRequiredStr paramTitle;
		[Export("paramTitle", ArgumentSemantic.Retain)]
		string ParamTitle { get; set; }

		// @property (retain, nonatomic) TCOptionalStr paramSummary;
		[Export("paramSummary", ArgumentSemantic.Retain)]
		string ParamSummary { get; set; }

		// @property (retain, nonatomic) TCOptionalStr paramDescription;
		[Export("paramDescription", ArgumentSemantic.Retain)]
		string ParamDescription { get; set; }

		// @property (retain, nonatomic) TCOptionalStr paramPics;
		[Export("paramPics", ArgumentSemantic.Retain)]
		string ParamPics { get; set; }

		// @property (retain, nonatomic) TCRequiredStr paramAct;
		[Export("paramAct", ArgumentSemantic.Retain)]
		string ParamAct { get; set; }

		// @property (retain, nonatomic) TCOptionalStr paramShareUrl;
		[Export("paramShareUrl", ArgumentSemantic.Retain)]
		string ParamShareUrl { get; set; }
	}

	// @interface TencentOAuth : NSObject
	[BaseType(typeof(NSObject))]
	interface TencentOAuth
	{
		// @property (copy, nonatomic) NSString * accessToken;
		[Export("accessToken")]
		string AccessToken { get; set; }

		// @property (copy, nonatomic) NSDate * expirationDate;
		[Export("expirationDate", ArgumentSemantic.Copy)]
		NSDate ExpirationDate { get; set; }

		[Wrap("WeakSessionDelegate")]
		TencentSessionDelegate SessionDelegate { get; set; }

		// @property (assign, nonatomic) id<TencentSessionDelegate> sessionDelegate;
		[NullAllowed, Export("sessionDelegate", ArgumentSemantic.Assign)]
		NSObject WeakSessionDelegate { get; set; }

		// @property (copy, nonatomic) NSString * localAppId;
		[Export("localAppId")]
		string LocalAppId { get; set; }

		// @property (copy, nonatomic) NSString * openId;
		[Export("openId")]
		string OpenId { get; set; }

		// @property (copy, nonatomic) NSString * redirectURI;
		[Export("redirectURI")]
		string RedirectURI { get; set; }

		// @property (retain, nonatomic) NSString * appId;
		[Export("appId", ArgumentSemantic.Retain)]
		string AppId { get; set; }

		// @property (retain, nonatomic) NSString * uin;
		[Export("uin", ArgumentSemantic.Retain)]
		string Uin { get; set; }

		// @property (retain, nonatomic) NSString * skey;
		[Export("skey", ArgumentSemantic.Retain)]
		string Skey { get; set; }

		// @property (copy, nonatomic) NSDictionary * passData;
		[Export("passData", ArgumentSemantic.Copy)]
		NSDictionary PassData { get; set; }

		// @property (assign, nonatomic) TencentAuthMode authMode;
		[Export("authMode", ArgumentSemantic.Assign)]
		TencentAuthMode AuthMode { get; set; }

		// +(NSString *)sdkVersion;
		[Static]
		[Export("sdkVersion")]
		
		string SdkVersion { get; }

		// +(NSString *)sdkSubVersion;
		[Static]
		[Export("sdkSubVersion")]
		
		string SdkSubVersion { get; }

		// +(BOOL)isLiteSDK;
		[Static]
		[Export("isLiteSDK")]
		
		bool IsLiteSDK { get; }

		// +(TencentAuthorizeState *)authorizeState;
		[Static]
		[Export("authorizeState")]
		
		unsafe TencentAuthorizeState AuthorizeState { get; }

		// +(QQVersion)iphoneQQVersion;
		[Static]
		[Export("iphoneQQVersion")]
		
		QQVersion IphoneQQVersion { get; }

		// -(id)initWithAppId:(NSString *)appId andDelegate:(id<TencentSessionDelegate>)delegate;
		[Export("initWithAppId:andDelegate:")]
		IntPtr Constructor(string appId, TencentSessionDelegate @delegate);

		// +(BOOL)iphoneQQInstalled;
		[Static]
		[Export("iphoneQQInstalled")]
		
		bool IphoneQQInstalled { get; }

		// +(BOOL)iphoneQQSupportSSOLogin;
		[Static]
		[Export("iphoneQQSupportSSOLogin")]
		
		bool IphoneQQSupportSSOLogin { get; }

		// +(BOOL)iphoneQZoneInstalled;
		[Static]
		[Export("iphoneQZoneInstalled")]
		
		bool IphoneQZoneInstalled { get; }

		// +(BOOL)iphoneQZoneSupportSSOLogin;
		[Static]
		[Export("iphoneQZoneSupportSSOLogin")]
		
		bool IphoneQZoneSupportSSOLogin { get; }

		// -(BOOL)authorize:(NSArray *)permissions;
		[Export("authorize:")]
		
		bool Authorize(NSObject[] permissions);

		// -(BOOL)authorize:(NSArray *)permissions inSafari:(BOOL)bInSafari;
		[Export("authorize:inSafari:")]
		
		bool Authorize(NSObject[] permissions, bool bInSafari);

		// -(BOOL)authorize:(NSArray *)permissions localAppId:(NSString *)localAppId inSafari:(BOOL)bInSafari;
		[Export("authorize:localAppId:inSafari:")]
		
		bool Authorize(NSObject[] permissions, string localAppId, bool bInSafari);

		// -(BOOL)incrAuthWithPermissions:(NSArray *)permissions;
		[Export("incrAuthWithPermissions:")]
		
		bool IncrAuthWithPermissions(NSObject[] permissions);

		// -(BOOL)reauthorizeWithPermissions:(NSArray *)permissions;
		[Export("reauthorizeWithPermissions:")]
		
		bool ReauthorizeWithPermissions(NSObject[] permissions);

		// +(BOOL)HandleOpenURL:(NSURL *)url;
		[Static]
		[Export("HandleOpenURL:")]
		bool HandleOpenURL(NSUrl url);

		// +(BOOL)CanHandleOpenURL:(NSURL *)url;
		[Static]
		[Export("CanHandleOpenURL:")]
		bool CanHandleOpenURL(NSUrl url);

		// +(NSString *)getLastErrorMsg;
		[Static]
		[Export("getLastErrorMsg")]
		
		string LastErrorMsg { get; }

		// -(NSString *)getServerSideCode;
		[Export("getServerSideCode")]
		
		string ServerSideCode { get; }

		// -(void)logout:(id<TencentSessionDelegate>)delegate;
		[Export("logout:")]
		void Logout(TencentSessionDelegate @delegate);

		// -(BOOL)isSessionValid;
		[Export("isSessionValid")]
		
		bool IsSessionValid { get; }

		// -(BOOL)getUserInfo;
		[Export("getUserInfo")]
		
		bool UserInfo { get; }

		// -(void)openSDKWebViewQQShareEnable;
		[Export("openSDKWebViewQQShareEnable")]
		void OpenSDKWebViewQQShareEnable();

		// -(BOOL)getListAlbum;
		[Export("getListAlbum")]
		
		bool ListAlbum { get; }

		// -(BOOL)getListPhotoWithParams:(NSMutableDictionary *)params;
		[Export("getListPhotoWithParams:")]
		bool GetListPhotoWithParams(NSMutableDictionary @params);

		// -(BOOL)addShareWithParams:(NSMutableDictionary *)params;
		[Export("addShareWithParams:")]
		bool AddShareWithParams(NSMutableDictionary @params);

		// -(BOOL)uploadPicWithParams:(NSMutableDictionary *)params;
		[Export("uploadPicWithParams:")]
		bool UploadPicWithParams(NSMutableDictionary @params);

		// -(BOOL)addAlbumWithParams:(NSMutableDictionary *)params;
		[Export("addAlbumWithParams:")]
		bool AddAlbumWithParams(NSMutableDictionary @params);

		// -(BOOL)checkPageFansWithParams:(NSMutableDictionary *)params;
		[Export("checkPageFansWithParams:")]
		bool CheckPageFansWithParams(NSMutableDictionary @params);

		// -(BOOL)addOneBlogWithParams:(NSMutableDictionary *)params;
		[Export("addOneBlogWithParams:")]
		bool AddOneBlogWithParams(NSMutableDictionary @params);

		// -(BOOL)addTopicWithParams:(NSMutableDictionary *)params;
		[Export("addTopicWithParams:")]
		bool AddTopicWithParams(NSMutableDictionary @params);

		// -(BOOL)setUserHeadpic:(NSMutableDictionary *)params;
		[Export("setUserHeadpic:")]
		bool SetUserHeadpic(NSMutableDictionary @params);

		// -(BOOL)setUserHeadpic:(NSMutableDictionary *)params andViewController:(UIViewController **)viewController;
		[Export("setUserHeadpic:andViewController:")]
		bool SetUserHeadpic(NSMutableDictionary @params, out UIViewController viewController);

		// -(BOOL)getVipInfo;
		[Export("getVipInfo")]
		
		bool VipInfo { get; }

		// -(BOOL)getVipRichInfo;
		[Export("getVipRichInfo")]
		
		bool VipRichInfo { get; }

		// -(BOOL)sendStory:(NSMutableDictionary *)params friendList:(NSArray *)fopenIdArray;
		[Export("sendStory:friendList:")]
		
		bool SendStory(NSMutableDictionary @params, NSObject[] fopenIdArray);

		// -(BOOL)sendAppInvitationWithDescription:(NSString *)desc imageURL:(NSString *)imageUrl source:(NSString *)source;
		[Export("sendAppInvitationWithDescription:imageURL:source:")]
		bool SendAppInvitationWithDescription(string desc, string imageUrl, string source);

		// -(BOOL)sendChallenge:(NSString *)receiver type:(NSString *)type imageURL:(NSString *)imageUrl message:(NSString *)message source:(NSString *)source;
		[Export("sendChallenge:type:imageURL:message:source:")]
		bool SendChallenge(string receiver, string type, string imageUrl, string message, string source);

		// -(BOOL)sendGiftRequest:(NSString *)receiver exclude:(NSString *)exclude specified:(NSString *)specified only:(BOOL)only type:(NSString *)type title:(NSString *)title message:(NSString *)message imageURL:(NSString *)imageUrl source:(NSString *)source;
		[Export("sendGiftRequest:exclude:specified:only:type:title:message:imageURL:source:")]
		bool SendGiftRequest(string receiver, string exclude, string specified, bool only, string type, string title, string message, string imageUrl, string source);

		// -(BOOL)cancel:(id)userData;
		[Export("cancel:")]
		bool Cancel(NSObject userData);

		// -(TCAPIRequest *)cgiRequestWithURL:(NSURL *)apiURL method:(NSString *)method params:(NSDictionary *)params callback:(id<TCAPIRequestDelegate>)callback;
		[Export("cgiRequestWithURL:method:params:callback:")]
		TCAPIRequest CgiRequestWithURL(NSUrl apiURL, string method, NSDictionary @params, TCAPIRequestDelegate callback);

		// -(BOOL)sendAPIRequest:(TCAPIRequest *)request callback:(id<TCAPIRequestDelegate>)callback;
		[Export("sendAPIRequest:callback:")]
		bool SendAPIRequest(TCAPIRequest request, TCAPIRequestDelegate callback);

		// -(NSString *)getUserOpenID;
		[Export("getUserOpenID")]
		
		string UserOpenID { get; }
	}

	// @protocol TencentLoginDelegate <NSObject>
	[BaseType(typeof(NSObject))]
	[Model]
	interface TencentLoginDelegate
	{
		// @required -(void)tencentDidLogin;
		[Abstract]
		[Export("tencentDidLogin")]
		void TencentDidLogin();

		// @required -(void)tencentDidNotLogin:(BOOL)cancelled;
		[Abstract]
		[Export("tencentDidNotLogin:")]
		void TencentDidNotLogin(bool cancelled);

		// @required -(void)tencentDidNotNetWork;
		[Abstract]
		[Export("tencentDidNotNetWork")]
		void TencentDidNotNetWork();

		// @optional -(NSArray *)getAuthorizedPermissions:(NSArray *)permissions withExtraParams:(NSDictionary *)extraParams;
		[Export("getAuthorizedPermissions:withExtraParams:")]
		NSObject[] GetAuthorizedPermissions(NSObject[] permissions, NSDictionary extraParams);
	}

	// @protocol TencentSessionDelegate <NSObject, TencentLoginDelegate, TencentApiInterfaceDelegate, TencentWebViewDelegate>
	[BaseType(typeof(NSObject))]
	[Model]
	interface TencentSessionDelegate : TencentLoginDelegate, TencentApiInterfaceDelegate, TencentWebViewDelegate
	{
		// @optional -(void)tencentDidLogout;
		[Export("tencentDidLogout")]
		void TencentDidLogout();

		// @optional -(BOOL)tencentNeedPerformIncrAuth:(TencentOAuth *)tencentOAuth withPermissions:(NSArray *)permissions;
		[Export("tencentNeedPerformIncrAuth:withPermissions:")]
		
		bool TencentNeedPerformIncrAuth(TencentOAuth tencentOAuth, NSObject[] permissions);

		// @optional -(BOOL)tencentNeedPerformReAuth:(TencentOAuth *)tencentOAuth;
		[Export("tencentNeedPerformReAuth:")]
		bool TencentNeedPerformReAuth(TencentOAuth tencentOAuth);

		// @optional -(void)tencentDidUpdate:(TencentOAuth *)tencentOAuth;
		[Export("tencentDidUpdate:")]
		void TencentDidUpdate(TencentOAuth tencentOAuth);

		// @optional -(void)tencentFailedUpdate:(UpdateFailType)reason;
		[Export("tencentFailedUpdate:")]
		void TencentFailedUpdate(UpdateFailType reason);

		// @optional -(void)getUserInfoResponse:(APIResponse *)response;
		[Export("getUserInfoResponse:")]
		void GetUserInfoResponse(APIResponse response);

		// @optional -(void)getListAlbumResponse:(APIResponse *)response;
		[Export("getListAlbumResponse:")]
		void GetListAlbumResponse(APIResponse response);

		// @optional -(void)getListPhotoResponse:(APIResponse *)response;
		[Export("getListPhotoResponse:")]
		void GetListPhotoResponse(APIResponse response);

		// @optional -(void)checkPageFansResponse:(APIResponse *)response;
		[Export("checkPageFansResponse:")]
		void CheckPageFansResponse(APIResponse response);

		// @optional -(void)addShareResponse:(APIResponse *)response;
		[Export("addShareResponse:")]
		void AddShareResponse(APIResponse response);

		// @optional -(void)addAlbumResponse:(APIResponse *)response;
		[Export("addAlbumResponse:")]
		void AddAlbumResponse(APIResponse response);

		// @optional -(void)uploadPicResponse:(APIResponse *)response;
		[Export("uploadPicResponse:")]
		void UploadPicResponse(APIResponse response);

		// @optional -(void)addOneBlogResponse:(APIResponse *)response;
		[Export("addOneBlogResponse:")]
		void AddOneBlogResponse(APIResponse response);

		// @optional -(void)addTopicResponse:(APIResponse *)response;
		[Export("addTopicResponse:")]
		void AddTopicResponse(APIResponse response);

		// @optional -(void)setUserHeadpicResponse:(APIResponse *)response;
		[Export("setUserHeadpicResponse:")]
		void SetUserHeadpicResponse(APIResponse response);

		// @optional -(void)getVipInfoResponse:(APIResponse *)response;
		[Export("getVipInfoResponse:")]
		void GetVipInfoResponse(APIResponse response);

		// @optional -(void)getVipRichInfoResponse:(APIResponse *)response;
		[Export("getVipRichInfoResponse:")]
		void GetVipRichInfoResponse(APIResponse response);

		// @optional -(void)sendStoryResponse:(APIResponse *)response;
		[Export("sendStoryResponse:")]
		void SendStoryResponse(APIResponse response);

		// @optional -(void)responseDidReceived:(APIResponse *)response forMessage:(NSString *)message;
		[Export("responseDidReceived:forMessage:")]
		void ResponseDidReceived(APIResponse response, string message);

		// @optional -(void)tencentOAuth:(TencentOAuth *)tencentOAuth didSendBodyData:(NSInteger)bytesWritten totalBytesWritten:(NSInteger)totalBytesWritten totalBytesExpectedToWrite:(NSInteger)totalBytesExpectedToWrite userData:(id)userData;
		[Export("tencentOAuth:didSendBodyData:totalBytesWritten:totalBytesExpectedToWrite:userData:")]
		void TencentOAuth(TencentOAuth tencentOAuth, nint bytesWritten, nint totalBytesWritten, nint totalBytesExpectedToWrite, NSObject userData);

		// @optional -(void)tencentOAuth:(TencentOAuth *)tencentOAuth doCloseViewController:(UIViewController *)viewController;
		[Export("tencentOAuth:doCloseViewController:")]
		void TencentOAuth(TencentOAuth tencentOAuth, UIViewController viewController);
	}

	// @protocol TencentWebViewDelegate <NSObject>
	[Protocol, Model]
	[BaseType(typeof(NSObject))]
	interface TencentWebViewDelegate
	{
		// @optional -(BOOL)tencentWebViewShouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation;
		[Export("tencentWebViewShouldAutorotateToInterfaceOrientation:")]
		bool TencentWebViewShouldAutorotateToInterfaceOrientation(UIInterfaceOrientation toInterfaceOrientation);

		// @optional -(NSUInteger)tencentWebViewSupportedInterfaceOrientationsWithWebkit;
		[Export("tencentWebViewSupportedInterfaceOrientationsWithWebkit")]
		
		nuint TencentWebViewSupportedInterfaceOrientationsWithWebkit { get; }

		// @optional -(BOOL)tencentWebViewShouldAutorotateWithWebkit;
		[Export("tencentWebViewShouldAutorotateWithWebkit")]
		
		bool TencentWebViewShouldAutorotateWithWebkit { get; }
	}
}
