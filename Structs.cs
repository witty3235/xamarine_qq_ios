﻿using System;

namespace QQ_SDK_Xamarin_iOS
{
	public enum QQApiSendResultCode
	{
		Sendsucess = 0,
		Qqnotinstalled = 1,
		Qqnotsupportapi = 2,
		Messagetypeinvalid = 3,
		Messagecontentnull = 4,
		Messagecontentinvalid = 5,
		Appnotregisted = 6,
		Appshareasync = 7,
		QqnotsupportapiWithErrorshow = 8,
		Sendfaild = -1,
		Qzonenotsupporttext = 10000,
		Qzonenotsupportimage = 10001,
		Versionneedupdate = 10002
	}

	public enum kQQAPICtrlFlagQ : uint
	{
		ZoneShareOnStart = 1,
		ZoneShareForbid = 2,
		QShare = 4,
		QShareFavorites = 8,
		QShareDataline = 16
	}

	public enum QQApiURLTargetType : uint
	{
		NotSpecified = 0,
		Audio = 1,
		Video = 2,
		News = 3
	}

	public enum QQApiInterfaceReqType : uint
	{
		Getmessagefromqqreqtype = 0,
		Sendmessagetoqqreqtype = 1,
		Showmessagefromqqreqtype = 2
	}

	public enum QQApiInterfaceRespType : uint
	{
		Showmessagefromqqresptype = 0,
		Getmessagefromqqresptype = 1,
		Sendmessagetoqqresptype = 2
	}

	public enum TCOLogLevel
	{
		Disabled = -1,
		Error = 0,
		Warning,
		Info,
		Debug
	}

	public enum QQVersion : uint
	{
		Uninstall,
		Version3_0,
		Version4_0,
		Version4_2_1,
		Version4_5,
		Version4_6,
		Version4_7
	}

	public enum ReponseResult : uint
	{
		Succeed = 0,
		Failed = 1
	}

	public enum UpdateFailType : uint
	{
		Unknown = 1,
		UserCancel,
		Network
	}

	public enum OpenSDKError
	{
		Invalid = -1,
		ErrorUnsupportedAPI = -2,
		ErrorSuccess = 0,
		ErrorUnknown,
		ErrorUserCancel,
		ErrorReLogin,
		ErrorOperationDeny,
		ErrorNetwork,
		ErrorURL,
		ErrorDataParse,
		ErrorParam,
		ErrorConnTimeout,
		ErrorSecurity,
		ErrorIO,
		ErrorServer,
		ErrorWebPage,
		ErrorUserHeadPicLarge = 65536
	}

	public enum TencentReqMessageType : uint
	{
		encentAppQueryContent,
		encentAppShowContent,
		hirdAppQueryContent,
		hirdAppShowContent
	}

	public enum TencentRespMessageType : uint
	{
		encentAppQueryContent,
		encentAppShowContent,
		hirdAppQueryContent,
		hirdAppShowContent
	}

	public enum TencentObjVersion : uint
	{
		TextObj,
		ImageObj,
		AudioObj,
		VideoObj,
		ImageAndVideoObj
	}

	public enum TencentApiImageSourceType : uint
	{
		AllImage,
		UrlImage,
		DataImage
	}

	public enum TencentApiVideoSourceType : uint
	{
		AllVideo,
		LocalVideo,
		NetVideo
	}

	public enum TecnentPlatformType : uint
	{
		IphoneQQ,
		IphoneQZONE,
		ThirdApp
	}

	public enum TencentApiRetCode : uint
	{
		Success,
		PlatformUninstall,
		PlatformNotSupport,
		ParamsError,
		Fail
	}

	public enum TencentAuthorizeState : uint
	{
		NotAuthorizeState,
		SSOAuthorizeState,
		WebviewAuthorzieState
	}

	public enum TencentAuthMode : uint
	{
		ClientSideToken,
		ServerSideCode
	}
}

